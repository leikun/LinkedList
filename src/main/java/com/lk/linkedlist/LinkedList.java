package com.lk.linkedlist;


import java.util.NoSuchElementException;

/**
 * @ClassName LinkedList
 * @Description         linkedList采用的是双向链表,可以从first开始增删  也可以从last节点
 *                      这个类在jdk中继承了队列 所以具有队列的一些特性  比如先进先出  后进先出
 *                      如果增删不指定index下标的情况下  它的时间复杂度是O(1)  否则可能就是O(n)
 * @Author leikun
 * @Date2019/9/5 17:11
 **/
public class LinkedList<E> implements List<E> {

    transient int size;

    protected transient int modCount;

    /**
     * 第一个节点
     */
    private Node<E> first;

    /**
     * 最后一个节点
     */
    private Node<E> last;

    public LinkedList() {
    }


    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public boolean contains(Object o) {
        return indexOf(o) != -1;
    }

    /**
     * 查找元素下标 不存在就返回-1
     *
     * @param o
     * @return
     */
    public int indexOf(Object o) {
        int index = 0;
        if (o == null) {
            //循环链表  如果if成立则直接返回index下标
            for (Node<E> x = first; x != null; x = x.next) {
                if (x.item == null)
                    return index;
                index++;
            }
        } else {
            for (Node<E> x = first; x != null; x = x.next) {
                if (o.equals(x.item))
                    return index;
                index++;
            }
        }
        return -1;
    }

    @Override
    public boolean add(E e) {
        linkLast(e);
        return true;
    }

    /**
     * 以last节点进行新增
     *
     * @param e 新的节点对象
     */
    private void linkLast(E e) {
        //拿到最后一个节点  基于最后一个节点做新增
        final Node<E> l = last;
        //构造新节点  并且将这个新节点的上一个节点设置上一步操作的last节点
        final Node<E> newNode = new Node<>(e, l, null);
        //此时last节点应该是为我们新增的newNode节点对象
        last = newNode;
        //如果l节点  也就是last节点为null  说明这个链表是空的
        if (l == null) {
            //first节点就是第一次新增的newNode节点
            first = newNode;
        } else {
            //设置新增newNode之前的上个节点last节点  把last节点的下个节点设置为newNode节点即可
            l.next = newNode;
        }
        modCount++;
        size++;
    }

    @Override
    public boolean remove(Object o) {
        return false;
    }

    @Override
    public void clear() {
        //从第一个节点开始遍历
        for (Node<E> f = first; f != null; ) {
            //拿到下一个节点  定义一个临时节点
            Node<E> n = f.next;
            //把item:当前节点、prev:上一个节点、next:下一个节点
            f.item = null;
            f.prev = null;
            f.next = null;
            //f=n  就是f=我们定义的那个临时节点  也就是下个节点  如果f=null  循环也就结束了
            f = n;
        }
        size = 0;
        modCount++;
        first = last = null;
    }

    @Override
    public E get(int index) {
        checkElementIndex(index);
        return node(index).item;
    }

    /**
     * 根据index查找节点  查询的时间复杂度o(n)  虽然底层使用了二分法  但是如果index刚好是中间值此时遍历效率还是挺慢的
     *
     * @param index
     * @return
     */
    private Node<E> node(int index) {
        //1 2 3 4 5 6 7 8 9 10 11
        //进行二分法查找
        //相当于size/2  也就是获取中间值  得到一个中间的下标值
        int middle = size >> 1;
        //如果index小于中间值说明我们只需要从first节点一直开始循环遍历
        if (index < middle) {
            //从第一个节点开始查找  通过循环一直.next对象
            Node<E> f = first;
            //循环  一直到小于index结束
            // 这里可能有些人会有一些疑问  index不是从0开始的吗 为什么要小于  而不是小于等于
            //因为我们根据index只是查找当前index对应的节点对象  而在循环中  f=f.next  拿到的是当前节点的下一个节点对象  所以不能等于index
            for (int i = 0; i < index; i++) {
                //拿到下个节点  f=f.next重新赋值回去
                f = f.next;
            }
            return f;
        }
        //因为index大于中间值  所以从最后一个节点开始循环遍历
        Node<E> l = last;
        //10 9 8 7 6 5 4 3 2 1
        //这里的原理可以参考上面的  一样的操作   只是从f=f.next变成了 prev  prev就是拿到上个节点对象
        for (int i = size - 1; i > index; i--) {
            l = l.prev;
        }
        return l;
    }

    /**
     * 检查下标是否越界
     *
     * @param index
     */
    private void checkElementIndex(int index) {
        if (!isElementIndex(index))
            throw new IndexOutOfBoundsException("下标越界--->index:" + index);
    }

    private boolean isElementIndex(int index) {
        return index >= 0 && index < size;
    }

    @Override
    public E set(int index, E element) {
        checkElementIndex(index);
        //拿到原来的node节点对象
        Node<E> x = node(index);
        //原来的item对象
        E oldVal = x.item;
        //设置原来node.item=新的节点
        x.item = element;
        return oldVal;
    }

    @Override
    public void add(int index, E element) {
        checkPositionIndex(index);
        if (index == size) {
            linkLast(element);
        } else {
            //开始新增
            linkBefore(element, node(index));
        }
    }

    /**
     * 这个检查下标用用add方法 带index新增使用的
     *
     * @param index
     */
    private void checkPositionIndex(int index) {
        if (!isPositionIndex(index))
            throw new IndexOutOfBoundsException("下标越界--->index:" + index);
    }

    private boolean isPositionIndex(int index) {
        return index >= 0 && index <= size;
    }

    /**
     * 新增
     *
     * @param element 新增节点
     * @param node    根据index查询的节点
     */
    private void linkBefore(E element, Node<E> node) {
        // 1 2 3 4 5 6
        //拿到node节点的上一个节点  目的是构造newNode节点的时候  newNode的上一个节点是pred节点
        Node<E> pred = node.prev;
        //构造newNode节点  设置当前节点,上一个节点,下一个节点关系
        Node<E> newNode = new Node<>(element, pred, node);
        //设置node节点的上一个节点是newNode节点
        node.prev = newNode;
        //如果pred==null说明当前index可能是0  也就是第一个节点 此时基于index:0新增  first要变成newNode节点
        if (pred == null) {
            //设置新的first节点
            first = newNode;
        } else {
            //设置上一个节点的下个节点是新增节点
            pred.next = newNode;
        }
        size++;
        modCount++;
    }

    @Override
    public E remove(int index) {
        checkElementIndex(index);
        return unlink(node(index));
    }

    public E removeFirst() {
        final Node<E> f = first;
        if (f == null)
            throw new NoSuchElementException();
        return unlinkFirst(f);
    }

    /**
     * 从first开始删除  当然也可以从last开始删除  原理都是一样
     *
     * @param f first节点  也就是链表的头
     * @return
     */
    private E unlinkFirst(Node<E> f) {
        // 拿到当前节点
        final E element = f.item;
        //当前节点的下个节点
        final Node<E> next = f.next;
        //设置item、next为null
        f.item = null;
        f.next = null;
        //first=当前节点的下个节点
        first = next;
        //如果下个节点==null  说明需要给last节点赋值为null
        if (next == null)
            last = null;
        else
            //设置当前节点的上个节点=null  因为此时它已经是first节点  所以它的prev节点应该是null
            next.prev = null;
        size--;
        modCount++;
        return element;
    }

    /**
     * 解绑
     *
     * @param node 要删除的节点
     * @return
     */
    private E unlink(Node<E> node) {
        //1 2 3
        //删除节点的上个节点
        Node<E> prev = node.prev;
        //删除节点的下个节点
        Node<E> next = node.next;
        //删除节点的item对象  用于return返回
        E oldElement = node.item;
        //如果上个节点==null  需要改变first的值  此时fist应该=删除节点的下个节点
        if (prev == null) {
            first = next;
        } else {
            //将删除节点的上个节点的下个节点改为 删除节点的下个节点
            prev.next = next;
            //把当前节点的prev设置成null
            node.prev = null;
        }
        //next==null  需要改变last节点的值  此时last=删除节点的上个节点
        if (next == null) {
            last = prev;
        } else {
            //将删除节点的上个节点的值 改为 删除节点的上个节点
            next.prev = prev;
            //当前节点的next设置成null
            node.next = null;
        }
        //设置当前节点的item为null
        node.item = null;
        size--;
        modCount++;
        return oldElement;
    }

    private static class Node<E> {
        /**
         * 当前节点
         */
        E item;
        /**
         * 上一个节点
         */
        Node<E> prev;
        /**
         * 下一个节点
         */
        Node<E> next;

        public Node(E item, Node<E> prev, Node<E> next) {
            this.item = item;
            this.prev = prev;
            this.next = next;
        }
    }

    public static void main(String[] args) {
        LinkedList<Integer> linkedList = new LinkedList<>();
        for (int i = 0; i < 10; i++) {
            linkedList.add(i + 1);
        }
        System.out.println("新增节点1-10");
        for (int i = 0; i < linkedList.size(); i++) {
            System.out.print(linkedList.get(i) + "\t");
        }
        System.out.println("\n删除index=3的节点");
        linkedList.remove(3);
        for (int i = 0; i < linkedList.size(); i++) {
            System.out.print(linkedList.get(i) + "\t");
        }
        System.out.println("\n是否包含节点6,isFlag-->" + linkedList.contains(6));

        System.out.println("\n调用removeFirst循环删除");
        int size = linkedList.size;
        for (int i = 0; i < size; i++) {
            System.out.println("开始删除节点:" + linkedList.removeFirst());
        }
        System.out.println("\nsize:" + linkedList.size());

        System.out.println("根据下标0新增节点100");
        linkedList.add(0, 100);
        for (int i = 0; i < linkedList.size(); i++) {
            System.out.print(linkedList.get(i) + "\t");
        }
        System.out.println("\n在下标0的位置新增节点99");
        linkedList.add(0, 99);
        for (int i = 0; i < linkedList.size(); i++) {
            System.out.print(linkedList.get(i) + "\t");
        }
    }
}
